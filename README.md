This module is a service that transcodes a `.mp4` video into
an adaptive bitrate streaming format.
This module supports transcoding to the two most popular formats:
[HLS](https://developer.apple.com/streaming/) and [DASH](https://dashif.org/).
Two dependencies are used for transcoding video files:
the Drupal [php_ffmpeg](https://www.drupal.org/project/php_ffmpeg) module;
the [PHP-FFmpeg-video-streaming](https://github.com/aminyazdanpanah/PHP-FFmpeg-video-streaming) library (if you like this project, please ⭐️ Star it on Github).

**Note**: this is a backend module and it does video conversion.
For the frontend player you can build it easily with the tools
provided by Drupal, or you can reuse the player provided
by the `streaming_media_views` sub-moule.

The module has several options:
- Repository, you can choose where to save the encoded video in a
local location (`public://` or `private://`), or on a cloud.
- Choice of HLS or DASH streaming technique.
- Choice of bit-rates in which each video will be
fragmented (144p, 240p, 360p, 480p, 720p, 1080p, 2k or 4k).
- Duration in seconds of each video fragment.
- Extraction of thumbnails (`.jpg` and `.gif`) for each video segment.
- Choice of output video format: `.ts`, `.fmp4` or `.mp4`.
- Choice of encoding: H.264 or VP9.
- Support of subtitles (WebVTT) in different languages.

# 1 - Installation
1. Install FFMpeg in your system.
2. Install and enable [PHP FFmpeg](https://www.drupal.org/project/php_ffmpeg) Drupal module.
```
$ composer require drupal/php_ffmpeg
$ drush en php_ffmpeg
```

3. correctly configure the `php_ffmpeg` module
(`/admin/config/development/php-ffmpeg`).
For example if you use a common Linux distribution:
```
ffmpeg_library: /usr/bin/ffmpeg
ffprobe_binary: /usr/bin/ffprobe
timeout: 3600
threads: 12
```

4. Install [PHP-FFmpeg-video-streaming](https://github.com/aminyazdanpanah/PHP-FFmpeg-video-streaming) library
```
$ composer require aminyazdanpanah/php-ffmpeg-video-streaming
```

5. Install and enable this module
```
$ composer require drupal/streaming
$ drush en streaming
```

# 2 - How to use
You can use this form in different ways and in different scenarios.

## 2.1 - The simplest way
1. Install and enable this module;
2. activate the `Streaming Media Views` sub-module
(Media and Views modules are used to build the UI);
3. Add new video: "Content" -> "Media" -> "Add Media"
-> "Streaming Video", leave the `playlist` and` poster` fields blank, save.
4. from "Content -> Media" you can convert your videos;
5. choose the poster if necessary and save;
6. use `streaming/modules/streaming_media_views/templates/media--streaming-video.html.twig`
   as an example of a player for the frontend (VideoJS 7).

## 2.2 - Players
If you want to customize the frontend, below is a list of compatible players.

### 2.2.1 - Web players
DASH and HLS
- [Video.js](https://github.com/videojs/video.js) >8 (Recommended)
- [Plyr](https://github.com/sampotts/plyr)
- [DPlayer](https://github.com/MoePlayer/DPlayer)
- [MediaElement.js](https://github.com/mediaelement/mediaelement)
- [Clappr](https://github.com/clappr/clappr)
- [Shaka Player](https://github.com/google/shaka-player)
- [Flowplayer](https://github.com/flowplayer/flowplayer)

Only DASH
- [dash.js](https://github.com/Dash-Industry-Forum/dash.js)

Only HLS
- [hls.js](https://github.com/video-dev/hls.js)

### 2.2.2 - Android player

DASH and HLS
- [ExoPlayer](https://github.com/google/ExoPlayer)

### 2.2.3 - IOS Players

DASH
- [MPEGDASH IOS Player](https://github.com/MPEGDASHPlayer/MPEGDASH-iOS-Player)
- [Viblast SDK](https://github.com/Viblast/ios-player-sdk)

HLS
- [Player](https://github.com/piemonte/Player)

### 2.2.4 - GNU/Linux Distro, MacOsX, Windows

DASH and HLS
- [VLC media Player](https://github.com/videolan/vlc)
- [FFmpeg (ffplay)](https://github.com/FFmpeg/FFmpeg)


## 2.3 - Customizations
If you need to create a more complex user interface or have a more complex
infrastructure, you can use this module as a service.

Prepare the data to be sent as follows:
```
$settings = [
  'video-data' => [
    'uri'  => path|url       // /path/v.mp4 or https://example.com/v.mp4
    'name' => string         // movie.mp4
    'id'   => null|string    // "1" or "video-id"
    'uuid' => null|string    // uuid string
  ],
  'subtitles' => [
    'key-language' => [       // 'italian', 'english', 'spanish', 'chinese'
      'path' => string,       // /pat/to/sub/*.vtt (WebVTT format)
      'lang-name' => string,  // 'Italiano', 'English', 'Español', '中文'
      'lang-code' => string,  // 'it', 'en', 'es', 'ch'
      'default' => bool
    ],
    ...
  ],
  'custom-settings' => null|Drupal\Core\Config\Config,
  'refresh-if-exists' => null|true|false,
];
```
Send the data to the `streaming.transcode` service
```
$result = \Drupal::service('streaming.transcode')->video($settings);
```
You will receive the results in the following format:
```
[
  'status' => bool,
  'message' => string
  'playlist' => [
    'file_name' => string
    'path' => [
      'relative_base_path' => string
      'real_base_path' => string
      'module_folder' => string
      'video_folder' => string
      'folder_relative_path' => string
      'folder_real_path' => string
      'playlist_relative_path' => string
      'playlist_real_path' => string
    ],
    'url' => [
      'base_url' => string
      'folder_relative' => string
      'folder_real' => string
      'playlist_real_url' => string
    ],
  ],
  'thumbnails' => [
    'jpg' => [
      [ 'relative_path' => string, 'real_path' => string, 'url' => string ]
      [ ... ]
    ],
    'gif' => [
      [ 'relative_path' => string, 'real_path' => string, 'url' => string ]
      [ ... ]
    ],
  ],
  'settings' => [ ... ],
];
```
If you want to delete saved data,
use the `delete()` method in the same way as `video()`.
```
$result = \Drupal::service('streaming.transcode')->delete($settings);
```

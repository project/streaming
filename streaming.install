<?php

/**
 * @file
 * Install file streaming module.
 */

declare(strict_types = 1);

/**
 * Implements hook_requirements().
 */
function streaming_requirements($phase) {
  // Add services that are not available yet, but are required by the classes
  // above. The container rebuild after module installation should reset these
  // to what is defined in streaming.services.yml.
  if (!\Drupal::hasService('logger.channel.streaming')) {
    $streamingLogger = \Drupal::service('logger.factory')->get('streaming');
    \Drupal::getContainer()->set('logger.channel.streaming', $streamingLogger);
  }
  if (!\Drupal::hasService('cache.streaming')) {
    $streamingCache = \Drupal::service('cache_factory')->get('streaming');
    \Drupal::getContainer()->set('cache.streaming', $streamingCache);
  }
}

/**
 * Implements hook_uninstall().
 */
function streaming_uninstall($is_syncing) {
  \Drupal::service('cache.streaming')->invalidateAll();
  // Remove configuration.
  $configuration_installed = \Drupal::service('file_system')
    ->scanDirectory(__DIR__ . '/config/install/', ' /.+(yml)/', ['key' => 'name']);

  foreach ($configuration_installed as $key => $proprieties) {
    Drupal::configFactory()->getEditable($key)->delete();
  }
}

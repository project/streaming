<?php

namespace Drupal\streaming\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Streaming settings form.
 */
class StreamingSettingsForm extends ConfigFormBase {

  /**
   * Module config.
   *
   * @var string Config settings
   */
  const SETTINGS = 'streaming.settings';

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'streaming_admin_settings';
  }

  /**
   * {@inheritDoc}
   *
   * @return string[]
   *   Returns an array with names of configurations.
   */
  protected function getEditableConfigNames(): array {
    return [static::SETTINGS];
  }

  /**
   * {@inheritDoc}
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   *
   * @return array
   *   Array with form.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::SETTINGS);

    $form['info'] = [
      '#type' => 'markup',
      '#markup' => t('
        <p>
          Before customizing "Streaming" make sure to correctly configure "PHP-FFMpeg" module. Look at <a href="/admin/config/development/php-ffmpeg">PHP FFMpeg settings</a> to customize.
        </p>
      '),
    ];

    // Vertical tabs.
    $form['streaming'] = [
      '#type' => 'vertical_tabs',
      '#prefix' => '<h2><small>' . t('Streaming settings.') . '</small></h2>',
    ];

    /* Video repository output options */
    $form['repository'] = [
      '#type' => 'details',
      '#title' => t('Video stream repository'),
      '#description' => $this->t('Video stream repository options.'),
      '#open' => TRUE,
      '#group' => 'streaming',
      '#weight' => 0,
    ];
    $form['repository']['local'] = [
      '#type' => 'details',
      '#title' => $this->t('Local repository'),
      '#open' => TRUE,
      '#group' => 'repository',
    ];
    $form['repository']['local']['local_repository'] = [
      '#type' => 'select',
      '#title' => $this->t('Local repository type'),
      '#description' => $this->t('Select local repository. You can also choose not to save locally, make sure you configure the connection with the cloud correctly.'),
      '#options' => [
        'public' => $this->t('public://'),
        'private' => $this->t('private://'),
        'only-cloud' => $this->t('Only to cloud'),
      ],
      '#default_value' => $config->get('local_repository'),
    ];
    $form['repository']['local']['output_folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Stream output folder'),
      '#description' => $this->t(
        "Relative folder based in <em>public://</em> or <em>private://</em> and not start or end with '/', if not exists will be created. Example: <em>streaming</em>."
      ),
      '#default_value' => $config->get('output_folder'),
    ];
    $form['repository']['cloud'] = [
      '#type' => 'details',
      '#title' => $this->t('Cloud repository'),
      '#description' => $this->t('This feature will be implemented by version 2.'),
      '#open' => FALSE,
      '#group' => 'repository',
    ];
    $form['repository']['cloud']['server_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Cloud type'),
      '#description' => $this->t('Select cloud type.'),
      '#options' => [
        'local' => $this->t('Local'),
        'aws' => $this->t('Amazon AWS'),
        'google' => $this->t('Google Cloud'),
        'azure' => $this->t('Microsoft Azure'),
        'custom' => $this->t('Custom Cluod'),
      ],
      '#default_value' => $config->get('server_type'),
    ];
    $form['repository']['cloud']['cloud_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cloud domain'),
      '#description' => $this->t('The module must know the domain on which the videos will be published (with "/" finals). Remember to enable <strong>CORS domain headers</strong> in your webserver.'),
      '#default_value' => $config->get('cloud_domain'),
    ];

    /* Common output options */
    $form['common'] = [
      '#type' => 'details',
      '#title' => t('Video stream options'),
      '#description' => $this->t('Common video stream options'),
      '#open' => FALSE,
      '#group' => 'streaming',
      '#weight' => 0,
    ];

    /* Technique Options */
    $form['common']['technique_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Adaptive technique'),
      '#description' => $this->t('Specify the adaptive bitrate streaming technique.'),
      '#open' => TRUE,
      '#group' => 'common',
    ];
    $form['common']['technique_options']['technique_description'] = [
      '#type' => 'markup',
      '#markup' => t('
        <p>
          <a href="https://en.wikipedia.org/wiki/Dynamic_Adaptive_Streaming_over_HTTP"
            target="_blank"
            title="Dynamic Adaptive Streaming over HTTP">
          MPEG-DASH (or DASH)</a> is an international and open source standard.
          <a href="https://en.wikipedia.org/wiki/HTTP_Live_Streaming"
            target="_blank"
            title="HTTP Live Streaming">
            HLS</a> was <a
            href="https://developer.apple.com/streaming/"
            target="_blank"
            title="Apple docs"
            >developed by Apple</a> and has not been published as an international standard,
          even though it has wide support.
          All browsers except Safari support DASH. All browsers support HSL.
        </p>
      '),
    ];
    $form['common']['technique_options']['technique'] = [
      '#type' => 'select',
      '#title' => $this->t('Streaming technique'),
      '#description' => $this->t('Select streaming technique'),
      '#options' => [
        'hls' => $this->t('HLS (recommended)'),
        'dash' => $this->t('DASH'),
      ],
      '#default_value' => $config->get('technique'),
    ];

    /* Adaptive bitrate Options */
    $form['common']['bitrate'] = [
      '#type' => 'details',
      '#title' => $this->t('Adaptive bitrate'),
      '#description' => $this->t('Enable/disable video resolutions.'),
      '#open' => TRUE,
      '#group' => 'common',
    ];
    foreach (\Drupal::service('streaming.representations')->getRepresentations() as $key => $item) {
      $form['common']['bitrate'][$key] = [
        '#type' => 'checkbox',
        '#title' => $this->t(
          '@l - @wx@h / @kb Kbps.',
          [
            // Label.
            '@l' => $item['l'],
            // Width.
            '@w' => $item['w'],
            // Height.
            '@h' => $item['h'],
            // KiloBitrate.
            '@kb' => $item['kb'],
          ]
        ),
        '#default_value' => $config->get($key),
      ];
    }

    /* Segment Options */
    $form['common']['segment'] = [
      '#type' => 'details',
      '#title' => $this->t('Video segment'),
      '#description' => $this->t('Common settings for video segments.'),
      '#open' => TRUE,
      '#group' => 'common',
    ];
    $form['common']['segment']['segment_duration'] = [
      '#type' => 'number',
      '#title' => $this->t('Segment duration'),
      '#description' => $this->t('Video segments durations in seconds (default is 10).'),
      '#min' => 3,
      '#step' => 1,
      '#max' => 60,
      '#default_value' => $config->get('segment_duration'),
    ];
    $form['common']['segment']['thumbnail'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Extract a thumbnail at the first second of each segment.'),
      '#default_value' => $config->get('thumbnail'),
    ];
    $form['common']['segment']['thumbnail_gif'] = [
      '#type' => 'number',
      '#title' => $this->t('Thumbnail GIF'),
      '#description' => $this->t("Extract a GIF thumbnail for <em>x</em> second of each segment. Set '0' to disable. Do not exceed the number of seconds of each fragment. For example: set '2' to extract a 2 second gif for each segment."),
      '#min' => 0,
      '#step' => 1,
      '#max' => $config->get('segment_duration') - 1,
      '#default_value' => $config->get('thumbnail_gif'),
    ];

    /* HLS options */
    $form['hls'] = [
      '#type' => 'details',
      '#title' => t('HLS options'),
      '#description' => $this->t('HLS specific options'),
      '#open' => FALSE,
      '#group' => 'streaming',
      '#weight' => 0,
    ];
    $form['hls']['segment'] = [
      '#type' => 'details',
      '#title' => $this->t('Video segment'),
      '#description' => $this->t('Common settings for HLS video segments.'),
      '#open' => TRUE,
      '#group' => 'hls',
    ];
    $form['hls']['segment']['hls_segment_video_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Video format'),
      '#description' => $this->t('Video format of each segment. The MP4 format was introduced in the year 2017 with the release of HLS 7 (RFC8216).'),
      '#options' => [
        'ts' => $this->t('.ts (recommended)'),
        'mp4' => $this->t('.mp4 (only recent player)'),
      ],
      '#default_value' => $config->get('hls_segment_video_format'),
    ];
    $form['hls']['segment']['hls_segment_video_encoding'] = [
      '#type' => 'select',
      '#title' => $this->t('Video encoding'),
      '#description' => $this->t('Video encoding of each segment.'),
      '#options' => [
        'h264' => $this->t('H.264'),
      ],
      '#default_value' => $config->get('hls_segment_video_encoding'),
    ];
    $form['hls']['segment']['hls_allow_cache'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable HLS video cache (default is checked).'),
      '#default_value' => $config->get('hls_allow_cache'),
    ];

    /* DASH options */
    $form['dash'] = [
      '#type' => 'details',
      '#title' => t('DASH options'),
      '#description' => $this->t('DASH specific options'),
      '#open' => FALSE,
      '#group' => 'streaming',
      '#weight' => 0,
    ];
    $form['dash']['segment'] = [
      '#type' => 'details',
      '#title' => $this->t('Video segment'),
      '#description' => $this->t('Common settings for DASH video segments.'),
      '#open' => TRUE,
      '#group' => 'dash',
    ];
    $form['dash']['segment']['dash_segment_video_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Video format'),
      '#description' => $this->t('Video format of each segment.'),
      '#options' => [
        'fmp4' => $this->t('.fmp4'),
      ],
      '#default_value' => $config->get('dash_segment_video_format'),
    ];
    $form['dash']['segment']['dash_segment_video_encoding'] = [
      '#type' => 'select',
      '#title' => $this->t('Video encoding'),
      '#description' => $this->t('Video encoding of each segment.'),
      '#options' => [
        'h264' => $this->t('H.264 (recommended)'),
        'vp9' => $this->t('VP9'),
      ],
      '#default_value' => $config->get('dash_segment_video_encoding'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('local_repository', $form_state->getValue('local_repository'))
      ->set('output_folder', $form_state->getValue('output_folder'))
      ->set('server_type', $form_state->getValue('server_type'))
      ->set('cloud_domain', $form_state->getValue('cloud_domain'))
      ->set('technique', $form_state->getValue('technique'))
      ->set('segment_duration', $form_state->getValue('segment_duration'))
      ->set('thumbnail', $form_state->getValue('thumbnail'))
      ->set('thumbnail_gif', $form_state->getValue('thumbnail_gif'))
      ->set('hls_segment_video_format', $form_state->getValue('hls_segment_video_format'))
      ->set('hls_segment_video_encoding', $form_state->getValue('hls_segment_video_encoding'))
      ->set('hls_allow_cache', $form_state->getValue('hls_allow_cache'))
      ->set('dash_segment_video_format', $form_state->getValue('dash_segment_video_format'))
      ->set('dash_segment_video_encoding', $form_state->getValue('dash_segment_video_encoding'));

    foreach (\Drupal::service('streaming.representations')->getRepresentations() as $key => $item) {
      $this->configFactory->getEditable(static::SETTINGS)
        ->set($key, $form_state->getValue($key));
    }

    $this->configFactory->getEditable(static::SETTINGS)->save();

    parent::submitForm($form, $form_state);
  }

}

<?php

namespace Drupal\streaming\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Coordinate\Dimension;
use Streaming\Media;
use Streaming\HLSSubtitle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Yaml;

/**
 * This service transcode media files.
 */
class TranscodingService {

  use StringTranslationTrait;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * Logger channel for this service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   *   The registered logger for this channel.
   */
  private LoggerChannelInterface $logger;

  /**
   * The config object providing the module's config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $config;

  /**
   * Array with video metadata.
   *
   * @var array
   */
  private array $videoData;

  /**
   * If enabled is an array that contains subtitles.
   *
   * @var mixed
   */
  private $subtitles;

  /**
   * Data file name.
   *
   * @var string
   */
  private string $dataFileName = "streaming.yaml";

  /**
   * Constructs the factory object with injected dependencies.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache interface.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger interface.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   ConfigFactory interface.
   */
  public function __construct(CacheBackendInterface $cache, LoggerChannelInterface $logger, ConfigFactoryInterface $configFactory) {
    $this->cache = $cache;
    $this->logger = $logger;
    $this->config = $configFactory->getEditable('streaming.settings');
  }

  /**
   * This method transcode a video.
   *
   * @param array $settings
   *   An array with settings = [
   *    'video-data' => [
   *     'uri'  => path|url,      // /path/v.mp4 or https://example.com/v.mp4
   *     'name' => string,        // movie.mp4
   *     'id'   => null|string    // "1" or "video-id"
   *     'uuid' => null|string    // uuid string
   *    ],
   *    'subtitles' => [
   *     'key-language' => [       // 'italian', 'english', 'spanish', 'chinese'
   *       'path' => string,       // /pat/to/sub/*.vtt (WebVTT format)
   *       'lang-name' => string,  // 'Italiano', 'English', 'Español', '中文'
   *       'lang-code' => string,  // 'it', 'en', 'es', 'ch'
   *       'default' => bool
   *     ],
   *    ],
   *    'custom-settings'   => null|Drupal\Core\Config\Config,
   *    'refresh-if-exists' => null|true|false,
   *    'console-progress'  => null|true|false,
   *   ].
   *
   * @return array
   *   Video encoded result = [
   *     'status' => bool,
   *     'message' => string
   *     'playlist' => [
   *       'file_name' => string
   *       'path' => [
   *          'relative_base_path' => string
   *          'real_base_path' => string
   *          'module_folder' => string
   *          'video_folder' => string
   *          'folder_relative_path' => string
   *          'folder_real_path' => string
   *          'playlist_relative_path' => string
   *          'playlist_real_path' => string
   *        ],
   *       'url' => [
   *          'base_url' => string
   *          'folder_relative' => string
   *          'folder_real' => string
   *          'playlist_real_url' => string
   *        ],
   *      ],
   *     'thumbnails' => [
   *       'jpg' => [
   *          [
   *            'relative_path' => string,
   *            'real_path' => string,
   *            'url' => string
   *          ],
   *          [ ... ]
   *       ],
   *       'gif' => [
   *          [
   *            'relative_path' => string,
   *            'real_path' => string,
   *            'url' => string
   *          ],
   *          [ ... ]
   *       ],
   *     ],
   *     'settings' => array
   *   ].
   *
   * @throws \Exception
   */
  public function video(array $settings): array {

    $check = $this->checkSettings($settings);
    if (!$check['status']) {
      $this->logger->error($check['message']);
      throw new \Exception($check['message']);
    }

    // Check if video is encoded.
    if ((new Filesystem)->exists([
      $this->outputPath()['folder_real_path'],
      $this->outputPath()['folder_real_path'] . $this->dataFileName,
    ])) {
      if (isset($settings['refresh-if-exists']) && $settings['refresh-if-exists'] === TRUE) {
        // Remove video folder.
        (new Filesystem)->remove($this->outputPath()['folder_real_path']);
        // Unset variable, loop issue in data dumpfile.
        unset($settings['refresh-if-exists']);
      }
      else {
        return (new Parser)->parsefile($this->outputPath()['folder_real_path'] . $this->dataFileName);
      }
    }

    try {
      // Load php_ffmpeg.
      $ffmpeg = \Drupal::service('php_ffmpeg');
      $video = $ffmpeg->open($this->videoData['uri']);

      // HLS Options.
      if ($this->config->get('technique') == 'hls') {
        $stream = $video
          ->hls()
          ->setHlsTime($this->config->get('segment_duration'))
          ->setHlsBaseUrl($this->outputUrl()['folder_real'])
          ->setHlsAllowCache((bool) $this->config->get('hls_allow_cache'))
          ->x264();

        // HLS video format.
        if ($this->config->get('hls_segment_video_format') == 'mp4') {
          $stream->fragmentedMP4();
        }

        // HLS subtitles.
        if ($this->subtitles !== FALSE) {
          $stream->subtitles($this->subtitles);
        }
      }
      // DASH options.
      else {
        $stream = $video
          ->dash()
          ->setSegDuration($this->config->get('segment_duration'))
          ->setAdaption('id=0,streams=v id=1,streams=a');

        // DASH video encoding.
        if ($this->config->get('dash_segment_video_encoding') == 'h264') {
          $stream->x264();
        }
        else {
          $stream->vp9();
        }
      }

      $stream->addRepresentations(
        \Drupal::service('streaming.representations')->makeRepresentations()
      );

      // Console progress output
      if (isset($settings['console-progress']) && $settings['console-progress'] === TRUE) {
        $format = $stream->getFormat();
        $format->on('progress', function ($stream, $format, $percentage) {
          echo sprintf(
            "\r Transcoding... (%s%%)[%s%s]",
            $percentage,
            str_repeat('#', $percentage),
            str_repeat('-', (100 - $percentage))
          );
        });
        $stream->setFormat($format);
      }

      // Save playlist.
      $stream->save($this->outputPath()['playlist_real_path']);

      $thumbnails = $this->getThumbnails($video);

      $result = [
        'status' => TRUE,
        'message' => $this->t('Video transcoding success.'),
        'playlist' => [
          'file_name' => $this->playlistName(),
          'path'  => $this->outputPath(),
          'url'   => $this->outputUrl(),
        ],
        'thumbnails' => $thumbnails,
        'settings' => $settings,
      ];

      // Save a result in yaml file.
      file_put_contents(
        $this->outputPath()['folder_real_path'] . $this->dataFileName,
        Yaml::dump($result, 2, 2)
      );

      return $result;

    }
    catch (\Exception $e) {
      $message = $this->t(
        "Error: Could not transcode video. Check FFMpeg's log. @EMessage",
        ['@EMessage' => $e->getMessage()]
      );
      $this->logger->error($message);
      return ['status' => FALSE, 'message' => $message];
    }
  }

  /**
   * This method delete a video streaming.
   *
   * @param array $settings
   *   Same settings video array settings.
   *
   * @return array
   *   Same return data video method.
   *
   * @throws \Exception
   */
  public function delete(array $settings): array {
    $check = $this->checkSettings($settings);
    if (!$check['status']) {
      $this->logger->error($check['message']);
      throw new \Exception($check['message']);
    }

    // Check if video is encoded.
    if ((new Filesystem)->exists([
      $this->outputPath()['folder_real_path'],
      $this->outputPath()['folder_real_path'] . $this->dataFileName,
    ])) {
      // Remove video folder.
      (new Filesystem)->remove($this->outputPath()['folder_real_path']);
    }

    return [
      'status' => TRUE,
      'message' => $this->t('Playlist and streaming data deleted.'),
      'playlist' => [
        'file_name' => $this->playlistName(),
        'path'  => $this->outputPath(),
        'url'   => $this->outputUrl(),
      ],
    ];
  }

  /**
   * Check and import settings.
   *
   * @param array $settings
   *   Array with settings to import.
   *
   * @return array
   *   Array with status and message.
   */
  private function checkSettings(array $settings): array {

    if (isset($settings['video-data'])) {
      $this->videoData = $settings['video-data'];
    }
    else {
      $message = $this->t('The $settings["video-data"] fields are empty, they should be filled in.');
      return ['status' => FALSE, 'message' => $message];
    }

    if (
      !isset($settings['video-data']['uri']) ||
      !is_string($settings['video-data']['uri']) ||
      !isset($settings['video-data']['name']) ||
      !is_string($settings['video-data']['name'])
    ) {
      $message = $this->t('["video-data"]["uri"] or ["video-data"]["name"] are empty or isn\'t a string');
      return ['status' => FALSE, 'message' => $message];
    }

    if (
      isset($settings['video-data']['id']) &&
      !is_string($settings['video-data']['id'])
    ) {
      $message = $this->t('$settings["video-data"]["id"] must be a string and it isn\'t.');
      return ['status' => FALSE, 'message' => $message];
    }

    if (
      isset($settings['video-data']['uuid']) &&
      !is_string($settings['video-data']['uuid'])
    ) {
      $message = $this->t('$settings["video-data"]["id"] must be a string and it isn\'t.');
      return ['status' => FALSE, 'message' => $message];
    }

    if (isset($settings['custom-settings'])) {
      $this->config = $settings['custom-settings'] instanceof Config
        ? $settings['custom-settings']
        : $this->config;
    }

    if (isset($settings['subtitles']) && is_array($settings['subtitles'])) {
      $this->subtitles = [];
      foreach ($settings['subtitles'] as $key => $item) {
        if (
          !isset($item['path'])       || !is_string($item['path']) ||
          !isset($item['lang-name'])  || !is_string($item['lang-name']) ||
          !isset($item['lang-code'])  || !is_string($item['lang-code'])
        ) {
          $message = $this->t('$settings["video-data"]["subtitle"] malformed.');
          return ['status' => FALSE, 'message' => $message];
        }

        $sub = new HLSSubtitle($item['path'], $item['lang-name'], $item['lang-code']);

        if ($item['default'] === TRUE) {
          $sub->default();
        }

        $this->subtitles[] = $sub;
      }
    }
    else {
      $this->subtitles = FALSE;
    }

    return ['status' => TRUE, 'message' => 'Well done!'];
  }

  /**
   * Make an output path.
   *
   * @return string[]
   *   Returns an array string with various path.
   */
  protected function outputPath(): array {

    $local_repository = $this->getLocalRepository()['placeholder'];
    $real_base_path = $this->getLocalRepository()['real_path'];

    $module_folder = $this->config->get('output_folder') . '/';
    $video_folder = $this->getVideoFolderName() . '/';
    $playlist_name = $this->playlistName();

    return [
      "relative_base_path" => $local_repository,
      "real_base_path" => $real_base_path,
      "module_folder" => $module_folder,
      "video_folder" => $video_folder,
      "folder_relative_path" => $local_repository . $module_folder . $video_folder,
      "folder_real_path" => $real_base_path . $module_folder . $video_folder,
      "playlist_relative_path" => $local_repository . $module_folder . $video_folder . $playlist_name,
      "playlist_real_path" => $real_base_path . $module_folder . $video_folder . $playlist_name,
    ];
  }

  /**
   * Make a output Url.
   *
   * @return string[]
   *   Returns an array with various urls.
   */
  protected function outputUrl(): array {

    $base_url = $this->config->get('server_type') != 'local'
      ? $this->config->get('cloud_domain')
      : $this->getLocalRepository()['url'];

    $folder_url = $this->outputPath()['module_folder'] . $this->outputPath()['video_folder'];

    return [
      "base_url" => $base_url,
      "folder_relative" => $folder_url,
      "folder_real" => $base_url . $folder_url,
      "playlist_real_url" => $base_url . $folder_url . $this->playlistName(),
    ];
  }

  /**
   * Returns the playlist type based on the chosen streaming technique.
   *
   * @return string
   *   Playlist extension.
   */
  private function playlistType(): string {
    return $this->config->get('technique') == 'hls' ? '.m3u8' : '.mpd';
  }

  /**
   * Returns the playlist file name.
   *
   * @return string
   *   Playlist file name
   */
  private function playlistName(): string {
    return $this->videoData['name'] . $this->playlistType();
  }

  /**
   * Make a video folder name.
   *
   * @return string
   *   Returns a string with a folder name.
   */
  private function getVideoFolderName(): string {

    if (isset($this->videoData['uuid']) && !empty($this->videoData['uuid'])) {
      $folderName = $this->videoData['uuid'];
    }
    elseif (isset($this->videoData['id']) && !empty($this->videoData['id'])) {
      $folderName = $this->videoData['id'];
    }
    else {
      $folderName = $this->videoData['name'];
    }

    return $folderName;
  }

  /**
   * This method return local repository in string format.
   *
   * @return array
   *   Local repository url and placeholder.
   */
  private function getLocalRepository(): array {
    $placeholder = $this->config->get('local_repository') == 'private'
      ? 'private://'
      : 'public://';

    // This includes '/' finals yet.
    $url = \Drupal::service('stream_wrapper_manager')
      ->getViaUri($placeholder)->getExternalUrl();

    $real_path = \Drupal::service('file_system')->realpath($placeholder) . '/';

    return [
      'placeholder' => $placeholder,
      'real_path' => $real_path,
      'url' => $url,
    ];
  }

  /**
   * This method extracts an image for each video segment.
   *
   * @param \Streaming\Media $video
   *   The video opened with FFMpeg.
   *
   * @return array
   *   An array with image path.
   */
  private function getThumbnails(Media $video): array {

    $thumbnails = [
      "jpg" => [],
      "gif" => [],
    ];

    $videoDuration = intval($video->hls()->metadata()->getFormat()->get('duration'));

    $timeStep = (int) $this->config->get('segment_duration');

    for ($i = 1; $i <= $videoDuration; $i += $timeStep) {

      if ((bool) $this->config->get('thumbnail') === TRUE) {
        $thumbnails['jpg'][] = $this->extractThumbnailJpg($video, $i);
      }
      if ((int) $this->config->get('thumbnail_gif') > 0) {
        $thumbnails['gif'][] = $this->extractThumbnailGif(
          $video,
          $i,
          [
            'width' => $video->hls()->metadata()->getVideoStreams()->first()->get('width'),
            'height' => $video->hls()->metadata()->getVideoStreams()->first()->get('height'),
          ],
          (int) $this->config->get('thumbnail_gif'),
        );
      }

    }

    return $thumbnails;
  }

  /**
   * Extract a jpg image from video.
   *
   * @param \Streaming\Media $video
   *   The video opened with FFMpeg.
   * @param int $startSecond
   *   The second to start extraction.
   *
   * @return array
   *   Path and url of image.
   */
  private function extractThumbnailJpg(Media $video, int $startSecond): array {

    $name = $this->videoData['name'] . '-' . $startSecond . '.jpg';
    $real_path = $this->outputPath()['folder_real_path'] . $name;

    $video
      ->frame(TimeCode::fromSeconds($startSecond))
      ->save($real_path);

    return [
      'relative_path' => $this->outputPath()['folder_relative_path'] . $name,
      'real_path' => $real_path,
      'url' => $this->outputUrl()['folder_real'] . $name,
    ];
  }

  /**
   * Extract a gif from video.
   *
   * @param \Streaming\Media $video
   *   The video opened with FFMpeg.
   * @param int $startSecond
   *   The second to start extraction.
   * @param array $dimensions
   *   Video dimensions.
   * @param int $duration
   *   GIF durations in seconds.
   *
   * @return array
   *   Path and url of gif.
   */
  private function extractThumbnailGif(Media $video, int $startSecond, array $dimensions, int $duration): array {

    $name = $this->videoData['name'] . '-' . $startSecond . '.gif';
    $real_path = $this->outputPath()['folder_real_path'] . $name;

    $video->gif(
      TimeCode::fromSeconds($startSecond),
      new Dimension(
        $dimensions['width'],
        $dimensions['height']
      ),
      $duration
    )->save($real_path);

    return [
      'relative_path' => $this->outputPath()['folder_relative_path'] . $name,
      'real_path' => $real_path,
      'url' => $this->outputUrl()['folder_real'] . $name,
    ];
  }

}

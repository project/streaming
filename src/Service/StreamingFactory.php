<?php

namespace Drupal\streaming\Service;

use Drupal\php_ffmpeg\PHPFFMpegFactory;
use Streaming\FFMpeg;

/**
 * This class customize drupal.php_ffmpeg service.
 */
class StreamingFactory extends PHPFFMpegFactory {

  /**
   * Logger channel that logs execution within FFMpeg extension to watchdog.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   *   The registered logger for this channel.
   */
  protected $logger;

  /**
   * Custom FFMpeg.
   *
   * @return \Streaming\FFMpeg
   *   FFMpeg stream
   */
  public function getFFMpeg() {
    return FFMpeg::create(
      parent::getFFMpegConfig(),
      $this->logger,
      parent::getFFMpegProbe()
    );
  }

}

<?php

namespace Drupal\streaming\Service;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Streaming\Representation;
use Symfony\Component\Yaml\Parser;

/**
 * This service returns an array of Representation object.
 */
class RepresentationService {

  /**
   * The config object providing the module's config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $config;

  /**
   * This is an array that mapping of "representations.yml" file.
   *
   * @var array
   *   [
   *      'key1' => ['kb' => '95', 'w' => '256', 'h' => '144', 'l' => '144p']
   *      'key2' => ...
   *    ]
   */
  protected array $representations;

  /**
   * The array contains representations enabled/disabled from module's config.
   *
   * @var array
   *  ['key1' => true, 'key2' => false, ... ]
   */
  protected array $representationFromConfig;

  /**
   * Constructs the factory object with injected dependencies.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   ConfigFactory interface.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->config = $configFactory->getEditable('streaming.settings');
    $this->setRepresentations();
    $this->setRepresentationFromConfig();
  }

  /**
   * Mapping all representation proprieties from "representations.yml".
   */
  public function setRepresentations() {
    $this->representations = (new Parser)->parsefile(__DIR__ . '/representations.yml');
  }

  /**
   * Returns all representations proprieties.
   *
   * @return array
   *   To know representation structure see "representation.yml".
   */
  public function getRepresentations(): array {
    return $this->representations;
  }

  /**
   * Use module's config to populate representationFromConfig array.
   */
  public function setRepresentationFromConfig() {
    foreach ($this->representations as $key => $item) {
      $this->representationFromConfig[$key] = $this->config->get($key);
    }
  }

  /**
   * Make an array of Representation object enabled in module's config.
   *
   * @return array
   *   Array of Representation object
   */
  public function makeRepresentations(): array {
    $r = [];

    foreach ($this->representationFromConfig as $key => $value) {
      if ($value) {
        $r[] = (new Representation)
          ->setKiloBitrate($this->representations[$key]['kb'])
          ->setResize($this->representations[$key]['w'], $this->representations[$key]['h']);
      }
    }

    return $r;
  }

}

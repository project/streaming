<?php

namespace Drupal\streaming\Service;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the php_ffmpeg service.
 */
class StreamingServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('php_ffmpeg')) {
      $definition = $container->getDefinition('php_ffmpeg');
      $definition->setClass('streaming\Service\StreamingFactory');
    }
  }

}

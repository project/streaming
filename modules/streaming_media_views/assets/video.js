(function (Drupal) {

  'use strict';

  Drupal.behaviors.videoJSLoaderStreaming = {
    attach: function (context) {
      // needed for making it work on Chrome based mobile phones properly
      let overrideNative = !videojs.browser.IS_ANY_SAFARI;

      let options = {
        responsive: true,
        controls: true,
        autoplay: false,
        aspectRatio: '16:9',
        fluid: true,
        caption: false,
        preload: "metadata",
        html5: {
          vhs: {
            overrideNative: overrideNative
          },
          nativeAudioTracks: !overrideNative,
          nativeVideoTracks: !overrideNative,
        }
      };

      let player = videojs(
        document.querySelector('.video-js'),
        options
      );
    }
  };

})(Drupal);

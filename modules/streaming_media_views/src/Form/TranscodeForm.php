<?php

namespace Drupal\streaming_media_views\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;

/**
 * This form set Media::streaming-video field with video streaming parameters.
 */
class TranscodeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'streaming_media_views_transcode';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $extra = NULL): array {
    $form['mid'] = [
      '#type' => 'hidden',
      '#value' => $extra['mid'],
      '#required' => TRUE,
    ];
    $form['playlist'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Playlist'),
      '#default_value' => $extra['playlist'],
      '#description' => $this->t('Default value: @playlist', ['@playlist' => $extra['playlist']]),
      '#disabled' => TRUE,
    ];
    if (isset($extra['poster'])) {
      $form['poster'] = [
        '#type' => 'radios',
        '#title' => $this->t('Choose video poster:'),
        '#options' => $extra['poster'],
        '#attributes' => ['class' => ['poster-radio']],
      ];
    }
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    $form['actions']['refresh'] = [
      '#type' => 'link',
      '#title' => $this->t('Refresh'),
      '#url' => Url::fromRoute('streaming.refresh', [
        'mid' => $extra['mid'],
      ]),
      '#attributes' => [
        'class' => [
          'button',
        ],
      ],
    ];
    $form['actions']['clear'] = [
      '#type' => 'link',
      '#title' => $this->t('Clear video streaming data'),
      '#url' => Url::fromRoute('streaming.clear', [
        'mid' => $extra['mid'],
      ]),
      '#attributes' => [
        'class' => [
          'button',
          'button--danger',
        ],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!is_int((int) $form_state->getValue('mid'))) {
      $form_state->setErrorByName('mid', $this->t('Unknown Media.'));
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $mediaObject = Media::load($form_state->getValue('mid'));
    $mediaObject->set('field_playlist', $form_state->getValue('playlist'));

    if ($form_state->getValue('poster')) {
      $file = File::create([
        'uri' => $form_state->getValue('poster'),
        'status' => 1,
      ]);
      $mediaObject->set('field_poster', $file);
    }

    $mediaObject->save();

    \Drupal::messenger()->addMessage(t("Success. Streaming options saved."));
    $form_state->setRedirect('view.media.media_page_list');
  }

}

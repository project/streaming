<?php

namespace Drupal\streaming_media_views\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Manage video content.
 */
class ContentController extends ControllerBase {

  /**
   * Transcode action.
   *
   * @param string $mid
   *   Media id.
   *
   * @return array
   *   Response data to template.
   */
  public function transcode(string $mid): array {

    $result = $this->exec($this->getParameters($mid));

    if ($result['status'] === TRUE) {
      $extra = [
        'mid' => $mid,
        'playlist' => $result['playlist']['url']['playlist_real_url'],
        'poster' => $this->preparePoster($result),
      ];

      // Setting poster to video preview.
      // I try to set the poster of `Media: streaming_video`,
      // otherwise I try to use the first generated thumbnails,
      // or as a last option I put FALSE and the player will use
      // the first frame of the video as poster.
      // Read from the bottom up.
      $result['media-poster-preview'] =
        // First thumbnail generated.
        $this->getMediaPoster($mid)['url'] === FALSE && isset($extra['poster'])
        ? \Drupal::service('stream_wrapper_manager')
          ->getViaUri(array_key_first($extra['poster']))->getExternalUrl()
        // Otherwise, media->field_poster['url'] or FALSE.
        : $this->getMediaPoster($mid)['url'];

      $form = \Drupal::formBuilder()->getForm('Drupal\streaming_media_views\Form\TranscodeForm', $extra);
    }
    else {
      $form = FALSE;
    }

    return [
      '#theme' => 'streaming-media-views-transcode',
      '#result' => $result,
      '#form' => $form,
    ];
  }

  /**
   * Refresh video streaming.
   *
   * @param string $mid
   *   Media id.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Response data to template.
   */
  public function refresh(string $mid): RedirectResponse {
    $videoParameters = $this->getParameters($mid);
    $videoParameters['refresh-if-exists'] = TRUE;

    $this->exec($videoParameters);

    return $this->redirect('streaming.transcode', ['mid' => $mid]);
  }

  /**
   * Clear video streaming data.
   *
   * @param string $mid
   *   Media id.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Response data to template.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function clear(string $mid): RedirectResponse {
    $videoParameters = $this->getParameters($mid);
    $videoParameters['refresh-if-exists'] = TRUE;

    $result = $this->exec($videoParameters, TRUE);

    if ($result['status']) {
      $mediaObject = Media::load($mid);
      $mediaObject->set('field_playlist', '');
      $mediaObject->set('field_poster', '');
      $mediaObject->save();
    }

    return $this->redirect('view.media.media_page_list');
  }

  /**
   * Delete media and video streaming data.
   *
   * @param string $mid
   *   Media id.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Response data to template.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function delete(string $mid): RedirectResponse {
    $videoParameters = $this->getParameters($mid);
    $videoParameters['refresh-if-exists'] = TRUE;

    $result = $this->exec($videoParameters, TRUE);

    if ($result['status']) {
      $mediaObject = Media::load($mid);
      $mediaObject->delete();
      \Drupal::messenger()->addMessage($this->t('Media id: @id deleted.', ['@id' => $mid]));
    }
    return $this->redirect('view.media.media_page_list');
  }

  /**
   * This method call "streaming.transcode" service.
   *
   * @param array $videoParameters
   *   Video parameters array.
   * @param bool $clear
   *   If clear video transcode data.
   *
   * @return array
   *   Result of video transcoding.
   */
  private function exec(array $videoParameters, bool $clear = FALSE): array {
    $result = $clear
      ? \Drupal::service('streaming.transcode')->delete($videoParameters)
      : \Drupal::service('streaming.transcode')->video($videoParameters);

    if ($result['status'] === TRUE) {
      if (isset($result['message']) && !empty($result['message'])) {
        \Drupal::messenger()->addMessage($result['message']);
      }
    }
    else {
      \Drupal::messenger()->addError($result['message']);
    }

    return $result;
  }

  /**
   * Get video parameters from Media.
   *
   * @param int $mid
   *   Media id.
   *
   * @return array
   *   Video data configuration.
   */
  private function getParameters(int $mid): array {
    $mediaObject = Media::load($mid);
    $file = File::load($mediaObject->field_media_video_file->target_id);

    return [
      'video-data' => [
        'uri'  => \Drupal::service('file_system')->realpath($file->getFileUri()),
        'name' => $file->getFilename(),
        'id'   => $file->id(),
        'uuid' => $file->uuid(),
      ],
    ];
  }

  /**
   * Prepare poster radio options.
   *
   * @param array $result
   *   Data width jpg and gif array.
   *
   * @return array|null
   *   If there are data, it returns the processed data.
   */
  private function preparePoster(array $result): ?array {
    $poster = [];

    foreach ($result['thumbnails']['jpg'] as $key => $item) {
      $poster[$item['relative_path']] = '<img src="' . $item['url'] . '" width="200" height="200" />';
    }
    foreach ($result['thumbnails']['gif'] as $key => $item) {
      $poster[$item['relative_path']] = '<img src="' . $item['url'] . '" width="200" height="200" />';
    }

    return count($poster) > 0 ? $poster : NULL;
  }

  /**
   * Return media poster if it is set.
   *
   * @param int $mid
   *   Media id.
   *
   * @return array
   *   Result array [ 'url' => string , 'uri' => string ]
   */
  private function getMediaPoster(int $mid): array {
    $mediaObject = Media::load($mid);
    $file = $mediaObject->field_poster->target_id
      ? File::load($mediaObject->field_poster->target_id)
      : FALSE;

    if ($file) {
      return [
        'url' => \Drupal::service('stream_wrapper_manager')
          ->getViaUri($file->getFileUri())->getExternalUrl(),
        'uri' => $file->getFileUri(),
      ];
    }
    else {
      return ['url' => FALSE, 'uri' => FALSE];
    }

  }

}

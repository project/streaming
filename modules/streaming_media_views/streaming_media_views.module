<?php

/**
 * @file
 * Streaming is a FFMpeg video streaming transcoder.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Implements hook_theme().
 */
function streaming_media_views_theme($existing, $type, $theme, $path) {
  return [
    'streaming-media-views-transcode' => [
      'variables' => ['result' => NULL, 'form' => NULL],
      'render element' => 'form',
    ],
  ];
}

/**
 * Implements hook_entity_operation().
 */
function streaming_media_views_entity_operation_alter(array &$operations, EntityInterface $entity) {
  $entityTypeId = $entity->getEntityTypeId();
  if ($entityTypeId !== 'media') {
    return;
  }
  $entityType = $entity->bundle();
  $entityId = $entity->id();
  if ($entityType === 'streaming_video') {
    $operations['streaming_transcode'] = [
      'title' => t('Transcode'),
      'weight' => -20,
      'url' => Url::fromRoute('streaming.transcode', ['mid' => $entityId]),
    ];
    $operations['streaming_clear'] = [
      'title' => t('Clear streaming data'),
      'weight' => -19,
      'url' => Url::fromRoute('streaming.clear', ['mid' => $entityId]),
    ];
    $operations['delete'] = [
      'title' => t('Delete media'),
      'url' => Url::fromRoute('streaming.delete', ['mid' => $entityId]),
    ];
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function streaming_media_views_theme_suggestions_file_video_alter(array &$suggestions, array $variables) {
  if (isset($variables['files'][0]['file'])) {
    $entityType = $variables['files'][0]['file']->_referringItem->getParent()->getEntity()->bundle();
    $suggestions[] = $variables['theme_hook_original'] . '__' . $entityType;
  }
}

/**
 * Implements hook_entity_presave().
 */
function streaming_media_views_media_presave(EntityInterface $entity) {
  if ($entity->bundle() == 'streaming_video') {
    if ($entity->field_poster->target_id) {
      $entity->thumbnail->target_id = $entity->field_poster->target_id;
    }

  }
}

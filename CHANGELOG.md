# 1.3.0
## Release notes
- Issue #3276890: unexpected error
- Issue #3295192: Don't white screen if ffmpeg can't be found
- Fix backend CSS
- Fix PHP Warning 'Undefined index uuid' in TranscodingService.php on line 466
and id on line 469
- New feature: progress output in console.
```
drush php
>>> \Drupal::service('streaming.transcode')->video([
  'video-data' => ['uri' => '/path/to/video.mp4', 'name' => 'video.mp4'],
  'console-progress' => TRUE
]);
```

# 1.2.0
## Release notes
- Issue #3264526: If a poster image is not chosen,
the playlist field is not saved.
- Changed upload progress indicator.
- New: sync thumbnail of media video-streaming with field poster.

# 1.1.4
## Release notes
- Issue #3264278: Error extracting thumbnail gif and jpg for videos longer
than 59 seconds.
- Changing the quality menu library with a better one.

# 1.1.3
## Release notes
Fix: added composer.json e declaring dependencies
to streaming_media_views sub-module

# 1.1.2
## Release notes
Fix: added correct `drupal/php_ffmpeg` dependencies

# 1.1.1
## Release notes
Issue #3262153: php_ffmpeg v0.19 break php_ffmpeg service

# 1.1.0
## Release notes
Added new feature:

- clear streaming data;
- delete streaming data files on media delete.

# 1.0.0
First release.
